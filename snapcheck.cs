﻿/* SnapCheck v2 (.NET port)
 * Copyright (C) 2014 Bytes Systems Integration (Pty) Ltd unless specified.
 * All Rights Reserved.
 * 
 * Makes use of the NetApp Manageability SDK.
 * Copyright (C) NetApp.
 * 
 * Checks for the presence of a specific snapshot on a volume and whether
 * the snapshot is recent.
 * 
 * 
 * */
using System;
using System.Collections.Generic;
using System.Text;
using NetApp.Manage;
using System.IO;

namespace snapcheck
{
    class SnapMan
    {
        

        static void Main(string[] args)
        {
            if (args.Length < 5) 
		        SnapChecker.Usage(args);

            string banner = "SnapCheck v2.11 (.NET port)\n(C) 2014 Bytes Technology Group (Pty) Ltd\nInternal Testing Only";
            Console.WriteLine(banner);
            Console.WriteLine();

            if (args.Length < 6)
                SnapChecker.Usage(args);

            /*String iniFileName = @"snapcheck.ini";
            IniParser parser = null;
            bool mailOnFailure = false;

            if (File.Exists(iniFileName))
            {
                parser = new IniParser(iniFileName);
                mailOnFailure = (parser.GetSetting("config", "mailonfailure") == "yes");
            }
            //IniParser parser = new IniParser(iniFileName);
            //bool mailOnFailure = (parser.GetSetting("config", "mailonfailure") == "yes");

            if (mailOnFailure)
            {
                
            }*/

            SnapChecker snapChecker = new SnapChecker(@"snapcheck.ini");


            snapChecker.CheckSnapshots(args);
                
            /*switch (args[0]) {
                case "-g":
                    GetSchedule(args);
                    break;
                case "-c":
                    CreateSnapshot(args);
                    break;
                case "-r":
                    RenameSnapshot(args);
                    break;
                case "-d":
                    DeleteSnapshot(args);
                    break;
                case "-l":
                    ListInfo(args);
                    break;
                default:
                    Usage(args);
                    break;
            }*/

        }
    }
}
