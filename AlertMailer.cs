﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;

namespace snapcheck
{
    public class AlertMailer
    {
        public AlertMailer(String from, String to, String server, String subject, String filer, String vol, String snapshot, String message)
        {
            this.from = from;
            this.to = to;
            this.server = server;
            this.subject = subject;
            this.filer = filer;
            this.vol = vol;
            this.snapshot = snapshot;
            this.message = message;
        }

        public String from { get; set; }
        public String to { get; set; }
        public String server { get; set; }
        public String subject { get; set; }
        public String body { get; set; }
        public String filer { get; set; }
        public String vol { get; set; }
        public String snapshot { get; set; }
        public String message { get; set; }

        public void Send()
        {
            SmtpClient client = new SmtpClient(server);
            MailMessage message = new MailMessage(from, to, subject, body);
            try
            {
                client.Send(message);
            }

            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
    }
}
