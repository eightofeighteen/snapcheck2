﻿using System;
using System.Collections.Generic;
using System.Text;
using NetApp.Manage;
using System.IO;

namespace snapcheck
{
    class SnapChecker
    {
        IniParser parser;
        bool shouldMailOnFailure;
        String mailFrom;
        String mailTo;
        String mailServer;
        AlertMailer mailer;
        String message;

        public SnapChecker()
        {

        }

        public SnapChecker(String iniFile)
        {
            if (File.Exists(iniFile))
            {
                Console.WriteLine("Using configuration file.");
                parser = new IniParser(iniFile);
                shouldMailOnFailure = (parser.GetSetting("config", "mailonfailure") == "yes");
                if (shouldMailOnFailure)
                {
                    mailFrom = parser.GetSetting("mail", "sender");
                    mailTo = parser.GetSetting("mail", "recipient");
                    mailServer = parser.GetSetting("mail", "server");
                    mailer = new AlertMailer(mailFrom, mailTo, mailServer, @"", @"", @"", @"", @"");
                }
            }
        }

        /// <summary>
        /// Structure to contain a single snapshot's name and creation date.
        /// </summary>
        public struct SnapshotItem
        {
            public string name
            {
                get;
                set;
            }
            public DateTime timestamp
            {
                get;
                set;
            }
        }

        private void WriteInfoLine(String info)
        {
            Console.WriteLine(info);
            message += (info + "\r\n");
        }

        private void WriteInfo(String info)
        {
            Console.Write(info);
            message += info;
        }

        /// <summary>
        /// Prints the usage summary and terminates.
        /// </summary>
        /// <param name="args">Program arguments.</param>
        static public void Usage(string[] args)
        {
            Console.WriteLine("SnapCheck <filer> <username> <password> <volume> <snapname> <distance>");
            System.Environment.Exit(-1);
        }

        /// <summary>
        /// Checks whether a snapshot exists in a list of snapshots.
        /// </summary>
        /// <param name="SnapshotList">List of snapshots to check.</param>
        /// <param name="snapname">Snapshot name we're looking for.</param>
        /// <returns>True is found, false if not found.</returns>
        public bool SnapshotExists(List<SnapshotItem> SnapshotList, string snapname)
        {
            foreach (SnapshotItem item in SnapshotList)
            {
                if (snapname == item.name)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Finds a specific snapshot in a shapshot list and returns it.
        /// If the snapshot is not in the list, the first is returned.
        /// The client is expected to use SnapshotExists to check if the snapshot exists.
        /// </summary>
        /// <param name="SnapshotList">List of snapshots to check.</param>
        /// <param name="snapname">Snapshot name we're looking for.</param>
        /// <returns>SnapshotItem of the specific snapshot.</returns>
        static public SnapshotItem FindSnapshot(List<SnapshotItem> SnapshotList, string snapname)
        {
            foreach (SnapshotItem item in SnapshotList)
                if (snapname == item.name)
                    return item;
            return SnapshotList[0];
        }

        /// <summary>
        /// Prints a list of snapshots in a justified format.
        /// </summary>
        /// <param name="SnapshotList">List of shapshots to print.</param>
        public void PrintSnapshotList(List<SnapshotItem> SnapshotList)
        {
            const int MIN_PAD = 3;
            if (SnapshotList.Count == 0)
                return;
            int twidth = SnapshotList[0].name.Length + SnapshotList[0].timestamp.ToString().Length + MIN_PAD;
            foreach (SnapshotItem item in SnapshotList)
            {
                int nwidth = item.name.Length + item.timestamp.ToString().Length + MIN_PAD;
                if (nwidth > twidth)
                    twidth = nwidth;

            }
            foreach (SnapshotItem item in SnapshotList)
            {
                WriteInfo(item.name);

                for (int i = 0; i < (twidth - item.name.Length - item.timestamp.ToString().Length); i++)
                {
                    WriteInfo(" ");
                }
                WriteInfoLine(item.timestamp.ToString());
            }
        }

        private void Terminate(int returnCode)
        {
            SendAlerts();
            System.Environment.Exit(returnCode);
        }

        void SendAlerts()
        {
            if (mailer != null && shouldMailOnFailure)
            {
                mailer.subject = "SnapCheck Failure on " + mailer.filer + "/" + mailer.vol + "/.snapshots/" + mailer.snapshot;
                mailer.body = message;
                mailer.Send();
            }
        }

        /// <summary>
        /// Checks for the suitability of a snapshot on a filer for backup purposes.
        /// </summary>
        /// <param name="args">Program arguments.</param>
        public void CheckSnapshots(string[] args)
        {
            string filer = args[0];                     // Filer hostname.
            string user = args[1];                      // Username to connect with.
            string pwd = args[2];                       // Password to connect with.
            string vol = args[3];                       // Volume to check.
            string snapname = args[4];                  // Snapshot name to check for.
            int distance = Convert.ToInt32(args[5]);    // Snapshot distance from the top of lits - not used in this port.

            if (mailer != null)
            {
                mailer.filer = filer;
                mailer.vol = vol;
                mailer.snapshot = snapname;
            }

            WriteInfoLine(String.Format("Filer: {0}, Volume: {1}", filer, vol));

            List<SnapshotItem> snapshotList = new List<SnapshotItem>();

            NaServer s;
            NaElement xi, xo;
            //
            // get the schedule
            //
            try
            {
                s = new NaServer(filer, 1, 0);
                s.Style = NaServer.AUTH_STYLE.LOGIN_PASSWORD;
                s.TransportType = NaServer.TRANSPORT_TYPE.HTTPS;
                s.Port = 443;
                s.SetAdminUser(user, pwd);

                xi = new NaElement("snapshot-list-info");
                xi.AddNewChild("volume", vol);
                xo = s.InvokeElem(xi);

                System.Collections.IList snapshots = xo.GetChildByName("snapshots").GetChildren();
                System.Collections.IEnumerator snapiter = snapshots.GetEnumerator();
                while (snapiter.MoveNext())
                {
                    NaElement snapshot = (NaElement)snapiter.Current;
                    int accesstime = snapshot.GetChildIntValue("access-time", 0);
                    DateTime datetime = new DateTime(1970, 1, 1, 0, 0, 0).AddSeconds(accesstime);
                    SnapshotItem item = new SnapshotItem();
                    item.name = snapshot.GetChildContent("name");
                    item.timestamp = datetime.ToLocalTime();
                    snapshotList.Add(item);
                }
                WriteInfoLine(String.Format("List of snapshots:"));

                PrintSnapshotList(snapshotList);
                
                if (!SnapshotExists(snapshotList, snapname))
                {
                    WriteInfoLine(String.Format("Snapshot {0} not found.", snapname));
                    WriteInfoLine("Type 1 failure.");
                    Terminate(1);
                }
                else
                {
                    WriteInfoLine(String.Format("Snapshot {0} found.", snapname));
                    TimeSpan diff = DateTime.Now.Subtract(FindSnapshot(snapshotList, snapname).timestamp);
                    if (diff.TotalSeconds >= (1 * 24 * 60 * 60))
                    {
                        WriteInfoLine("Type 3 failure.");
                        WriteInfoLine(String.Format("Snapshot {0} is {1} days old ({2} seconds).", snapname, diff, Math.Round(diff.TotalSeconds)));
                        Terminate(3);
                    }
                    WriteInfoLine(String.Format("Snapshot {0} is valid ({1} seconds old).", snapname, Math.Round(diff.TotalSeconds)));
                }

            }
            catch (NaAuthException e)
            {
                WriteInfoLine(String.Format("Authentication Error : " + e.Message));
                Terminate(-1);
            }
            catch (NaApiFailedException e)
            {
                WriteInfoLine(String.Format("API Failed : " + e.Message));
                Terminate(-1);
            }

            //SendAlerts(); // only send alerts on failure.

        }
    }
}
